/*
 * 
 * ===========================================================================================================================
 * 														Syntaxe de base en Java
 * 														
 * 														L1 - MIASHS - Algorithmique 1
 * 														TD créé par Gaël Guibon
 * ===========================================================================================================================
 * 
 * 
 * Ce fichier est dédié à la présentation de la syntaxe de base de Java (à ne pas confondre avec Javascript), directement dans le code.
 * Vous pouvez lancer l'ensemble ou utiliser ce fichier avec un debugger (voir la vidéo dédiée)
 * 
 * La plupart du contenu sera disponible sous forme de fichiers Java à ouvrir dans VSCode comme celui-ci. 
 * Cela vous permettra d'apprendre les notions directement par le code et les commentaires associés. D'ailleurs
 * ce que vous voyez ici est un commentaire multi-lignes, vous verrez des commentaires sur une ligne unique plus bas
 * par l'usage de la double barre oblique comme ceci : //
 * 
 * 
 * ❗️pour un apprentissage plus efficace, évitez les copier-collés. Tapez chaque caractère ! Cela travaillera votre mémoire musculaire et cognitive, 
 * vous facilitant grandement la vie par la suite.
 */

// import de la classe Scanner, utilisée dans les derniers exercices. Il faut mettre cette ligne en début de fichier pour pouvoir l'utiliser ensuite.
// Comme les lectures au clavier ne seront demandées qu'à partir de l'exercice 16, cette classe sera inutile avant cela.
import java.util.Scanner;

 // Je suis un commentaire sur une ligne. Vous me verrez beaucoup 😃
 
 /*
  * Je suis un commentaire multi-lignes. Vous me verrez régulièrement 😀
  */

/**
 * Je suis un commentaire multi-lignes qui peut servir de documentation.
 * Vous me verrez en détails surtout plus tard, dans les cours suivants de Java 🤓
 */
class Syntax { // ligne nécessaire pour créer une classe (nous ne verrons pas cela en cours mais c'est obligatoire pour faire exécuter le code)

	/*
	 * nous venons d'entrer dans un bloc, un bloc est déterminé par l'usage d'accolades 
	 * 	BLOC {
	 * 		intérieur du bloc
	 * 	} 
	 */

	public static void main (String[] args){ // fonction principale qui sert d'entrée au code. Pour le moment, nous n'utiliserons que celle-ci.
		// maintenant que les deux blocs obligatoires sont créés (classe + main) nous pouvons entrer notre code directement
	
		// créons une instruction qui va afficher une valeur en chaîne de caractères et faire un retour à la ligne (println signifie "print line")
		System.out.println("Coucou les MIASHS ! 😊"); // instruction 1
		// n'oubliez pas le point-virgule ! Chaque instruction se termine par un point virgule

		// créons une instruction qui va déclarer une variable de type String (chaîne de caractères, donc du texte)
		String prenom; // instruction 2

		// créons une instructions qui va initialiser la variable non en y affectant une valeur (ici, une chaîne de caractères)
		prenom = "Gaël"; // instruction 3
		// vous remarquerez que l'affectation de valeur se fait par le biais du signe "="
		
		// créons une instruction qui va déclarer et instancier une variable en une seule ligne
		String nom = "Guibon"; // instruction 4

		// Affichons les, une par ligne
		System.out.println(prenom); // instruction 5
		System.out.println(nom); // instruction 6

		// exécutez désormais ce code en cliquant sur "run" en haut de ce bloc (sous vscode) ou en haut à droite de l'interface.
	}

}


/*
 * ===========================================================================================================================
 * 														EXERCICES
 * ===========================================================================================================================
 */


/*
 * EXERCICES : 
 * Vous trouverez ci-dessous différents exercices visant à vous familiariser avec la syntaxe de base de Java.
 * Cette série de petits exercices minimalistes vise à vous exercer sur plusieurs notions de Java et de sa syntaxe,
 * mais aussi sur quelques unes des spécificités qui lui sont propres.
 * Il est important de bien maîtriser ces notions car elles seront nécessaires durant tout le cours (et toute votre vie ! 😇).
 * 
 * N'hésitez pas à vous inspirer de l'exemple expliqué ci-dessus ainsi que du cours.
 */

/**
 * Affichez votre prénom.
 * Pour ce premier exercice, ne déclarez pas de variable, faites juste un affichage de valeur.
 * 
 * 💡 Astuce : 
 * 		- au lieu de taper tout "System.out.println();", tapez "syso" puis la touche Entrée et voilàààà 🥳
 */
class TD01Exo1 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * 
 * Dans cet exercice, on ne vous demande pas d’affichage, juste de déclarer une variable nommée "maVariable" acceptant une chaîne de caractères.
 * 
 * 💡 Astuces :
 * 		- les noms de variables ne commencent JAMAIS pas une majuscule ! C'est une convention permettant de les distinguer des constantes et des classes.
 * 		- les noms de variables en Java suivent généralement le camelCase (maVariable), mais vous pourriez trouver du snake_case (ma_variable) ou du kebab-case (ma-variable) dans d'autres langages.
 */
class TD01Exo2 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une variable contenant votre nom et une variable contenant votre prénom.
 * Affichez chaque variable une par ligne.
 */
class TD01Exo3 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une constante contenant votre identifiant de l'université de Lorraine puis affichez la.
 * 
 * 💡Astuces :
 * 		- Si vous n'avez pas (encore) d'identifiant mettez 999999 par exemple
 * 		- Le mot clé des contantes est : final
 */
class TD01Exo4 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une variable nommée twitter dans laquelle vous affectez en un seul caractère le nouveau nom de Twitter.
 * 
 * 💡Astuce :
 * 		- les caractères sont un type à part entière de Java. 
 * 		- les caractères se distinguent par l'usage de guillemets simples ('' pour les char) et non doubles ("" pour les Strings)
 */
class TD01Exo5 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une variable comprenant votre âge et une variable comprenant un travail auquel vous aspirez (eh ouiii, faisons connaissance 😃).
 * Puis affichez les une à la fois.
 */
class TD01Exo6 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une variable contenant "Salut" et une autre contenant le prénom de votre voisin(e) / ami(e) / personnalité publique préférée.
 * Dans une nouvelle variable mettez la concaténation de ces deux chaînes de caractères.
 * Affichez ensuite cette variable.
 * 
 * Le résultat doit, par exemple, être : "Salut Gaël"
 * 
 * 💡 Astuces :
 * 		- la concaténation entre Strings s'effectue avec l'opérateur +
 * 		- n'oubliez pas qu'il faut idéalement avoir un espace entre les deux mots 😉
 */
class TD01Exo7 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une variable contenant l'année actuelle et une autre contenant votre année de naissance.
 * Mettez le résultat de la différence de ces deux variables dans une nouvelle variable.
 * Affichez la, précédée du texte "Le 31 décembre j'aurai " et suivie du texte " ans ! 🥲".
 * 
 * Ainsi, si la différence donne 23 alors le texte affiché sera : "Le 31 décembre j'aurai 23 ans ! 🥲"
 */
class TD01Exo8 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une variable contenant le nombre d'étudiants dans la salle et une contenant la note maximale (habituellement égale à 20).
 * Multipliez les et affectez le résultat à une nouvelle variable nommée "maximumGlobal".
 * Affichez la, précédée du texte "Notre promo doit essayer d'obtenir une note de ".
 * 
 * 💡 Astuce :
 * 		- l'opérateur de la multiplication est *
 */
class TD01Exo9 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une variable x de type entier (int) contenant le chiffre 5 et une variable y du même type contenant le chiffre 6.
 * Divisez la première par la seconde et affichez le résultat.
 * 
 * Que pouvez-vous dire du résultat ? 🧐
 */
class TD01Exo10 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une variable x de type double contenant le chiffre 5 et une variable y du même type contenant le chiffre 6.
 * Divisez la première par la seconde et affichez le résultat.
 * 
 * Que pouvez-vous dire du résultat ? 🧐
 */
class TD01Exo11 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Quid de la division entière sur des doubles ?
 * Créez de nouveau une variable x de type double contenant le chiffre 5 et une variable y du même type contenant le chiffre 6.
 * Divisez x par y puis utilisez la classe Math pour accéder aux fonctions mathématiques, et utilisez la fonction floor() qui permet de récupérer
 * la valeur entière d'un nombre.
 * 
 * Faites la division entière entre de x par y et affichez le résultat.
 * 
 * 💡 Astuces :
 * 		- Math.floor(2.4)  retourne la valeur entière 2 de type double
 * 		- La documentation de la classe Math est ici (en anglais) : https://docs.oracle.com/en/java/javase/20/docs/api/java.base/java/lang/Math.html
 * 		- la documentation de la méthode Math.floor() est ici (en anglais) : https://docs.oracle.com/en/java/javase/20/docs/api/java.base/java/lang/Math.html#floor(double)
 */
class TD01Exo12 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Une autre solution à ce problème est de le corriger à la racine : au niveau des types. Pour cela, sachez qu'il est possible de changer le type d'une variable
 * quand la valeur peut être interprétée dans un autre type. Nous appelons cette opération "caster". On dit donc qu'on cast une String en int par exemple.
 * 
 * Créez une variable de type entier (int) contenant votre niveau estimé en programmation (de 1 à 5).
 * Castez la en chaîne de caractères à l'aide de la syntaxe suivante :
 * 		(double) 9999
 * 
 * Et affectez le résultat dans une nouvelle variable du type de destination (en l'occurrence, double)
 * Enfin, affichez cette dernière variable.
 * 
 * Que constatez-vous ? 🧐
 */
class TD01Exo13 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Il existe cependant une exception au casting : les chaînes de caractères. (eh oui, ce serait trop simple autrement 😅)
 * 
 * Créez une variable de type double nommée "danse" contenant votre niveau estimé en danse 🕺💃 (de 1 à 5, mais avec décimales cette fois-ci 😉).
 * Si vous essayez de caster danse comme ceci :
 * 		(String) danse;
 * Cela ne fonctionnera pas. Il vous faut utiliser une méthode de classe String comme ceci :
 * 		String.valueOf( maVariable );
 * 
 * Castez votre niveau en danse et affichez le.
 * 
 * PS: vous remarquerez que quand nous avions concaténé un String avec un int, nous n'avons pas été obligé de caster le int en String. C'est
 * parce que le cast s'est fait implicitement 😉
 */
class TD01Exo14 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez une variable booléenne (boolean), ce qui signifie que son état est soit Vrai (True), soit faux (False). Cette variable aura la valeur indiquant
 * la réponse à cette question : "Avez-vous déjà programmé avant ce cours ?"
 * Affichez la.
 * 
 * Créez une variable booléenne répondant à la question : "Aimez-vous les litchis ?"
 * Affichez la.
 * 
 * PS: vous n'aimez pas les litchis ?... 😨... Non non non, vous AIMEZ les litchis mais ne le savez pas encore ! 😛
 */
class TD01Exo15 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Dans ce cours, vous allez souvent demander à l'utilisateur d'entrer une valeur, ce qui vous forcera à créer des algorithmes
 * indépendants des valeurs indiquées.
 * Toutefois, ils ne seront pas indépendants du type de ces valeurs. C'est pourquoi vous devez savoir comment récupérer l'entrée
 * clavier de l'utilisateur.
 * 
 * Pour ce faire, vous aurez besoin de la classe Scanner et de la classe System. 
 * 
 * Créez une instance de la classe Scanner afin de pouvoir l'utiliser pour récupérer et analyser ce que l'utilisateur
 * tape sur son clavier. Faites le comme ceci (les majuscules sont importantes !) :
 * 
 * 		Scanner scanner = new Scanner(System.in);
 * 
 * Tapez exactement cette ligne. Vous en aurez besoin, telle quelle, à chaque fois que vous voudrez utiliser le scanner. 
 * Nous ne détaillerons pas ici ce que cette fait précisément, concentrons-nous simplement sur son utilisation pour le moment.
 * Vous en saurez plus lors des cours suivants de Java. Sachez toutefois que cette ligne n'est à écrire qu'une seule fois par classe.
 * 
 * Une fois le scanner créé, utilisez le pour récupérer le contenu entré par l'utilisateur en tant que chaîne de caractères (String).
 * Pour ce faire, utilisez :
 * 
 * 		scanner.nextLine();
 * 
 * Affectez son résultat dans une variable puis affichez la.
 * Attention, il faut toujours fermer le scanner une fois qu'on en a plus besoin. Pour ce faire utilisez :
 * 
 * 		scanner.close();
 * 
 * 💡Astuce :
 * 		- il est de bonne pratique d'afficher une indication quand on attend une entrée utilisateur. Par exemple : "Veuillez entrer votre nom :"
 * 		- quand vous entrez du contenu, validez le à l'aide la touche Entrée du clavier... oui oui son nom vient de cet usage 🤓
 */
class TD01Exo16 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Vous avez vu comment récupérer une chaîne de caractères, mais si l'utilisateur tape un nombre ou un chiffre, nous aimerions directement
 * récupérer la valeur au bon type attendu.
 * Pour ce faire, plusieurs méthodes du scanner existent. Voici celles dont vous aurez le plus besoin dans ce cours :
 * 		scanner.nextLine();      // récupération de la valeur en une chaîne de caractères
 * 		scanner.nextInt();       // récupération de la valeur en un entier
 * 		scanner.nextDouble();    // récupération de la valeur en un double (donc avec décimales)
 * 		scanner.nextBoolean();   // récupération de la valeur en un booléen
 * 
 * Demandez l'âge de l'utilisateur et récupérez sa valeur en Double. Puis affichez la.
 */
class TD01Exo17 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/**
 * Créez un questionnaire qui va demander à l'utilisateur des informations et va les sauvegarder dans des variables.
 * Ensuite, affichez l'ensemble des éléments séparés par un espace.
 * 
 * Les informations à demander sont :
 * 		- nom
 * 		- prénom
 * 		- age
 * 		- livre préféré
 * 		- aimez-vous les kiwis 🥝 ?
 * 
 * 💡Astuce :
 * 		- Vous verrez que votre nextLine() après nextInt() sera ignoré. C'est parce que nextInt() ne saute pas de ligne. 
 * 		Il vous suffit alors d'utiliser un .nextLine() en plus pour passer à la ligne suivante 😉
 * 
 */
class TD01Exo18 {
	public static void main (String[] args) {
		// code moi steuplééé ! 🥺
	}
}

/*
 * Et voilà, vous avez fini le premier TD sur la syntaxe de Java ! 🎉
 * Ce TD est très important pour avoir les bases par la suite. aujourd'hui vous avez :
 * 		- installé VSCode et les extensions pour Java
 * 		- lancé vos premières exécutions de Java
 * 		- vu les bases de la syntaxe de Java (blocs, instructions, types, variables, etc.)
 * 
 */
